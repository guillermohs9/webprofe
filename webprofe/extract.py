from __future__ import print_function

import hashlib
import os
import io
import time
import subprocess
import rarfile
import fileinput
import sys
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload


# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive.metadata',
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file'
        ]


DBUSER = os.environ.get('DBUSER')
DBPASS = os.environ.get('DBPASS')
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))


def get_credentials():
    creds = None
    if os.path.exists(os.path.join(THIS_FOLDER, 'token.json')):
        creds = Credentials.from_authorized_user_file(os.path.join(THIS_FOLDER, 'token.json'), SCOPES)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(os.path.join(THIS_FOLDER, 'credentials.json'), SCOPES)
            creds = flow.run_local_server(port=0)
        with open(os.path.join(THIS_FOLDER, 'token.json'), 'w') as token:
            token.write(creds.to_json())
    return creds


def delete_file(file):
    try:
        os.remove(file)
    except OSError:
        pass


def rename_file(old_name, new_name):
    try:
        os.rename(old_name, new_name)
    except OSError:
        pass


def get_md5_hash(file):
    hash = hashlib.md5()
    with open(file, 'rb') as afile:
        buf = afile.read()
        hash.update(buf)
    return hash.hexdigest()


def replaceAll(file, searchExp, replaceExp):
    for line in fileinput.input(file, inplace=1):
        if searchExp in line:
            line = line.replace(searchExp, replaceExp)
        sys.stdout.write(line)


def extract_rar(file, dest_dir):
    r = rarfile.RarFile(file)
    r.extractall(path=dest_dir)
    r.close()


def backup_telefonos():
    command = ['mysql', '-sN', 'pfisnqn$profe', '-h', 'pfisnqn.mysql.pythonanywhere-services.com', '-u', DBUSER, f'-p{DBPASS}', '-e', 'select * from TELEFONO;']
    with open(os.path.join(THIS_FOLDER, 'backup'), 'w') as archivo_backup:
        proc = subprocess.call(command, stderr=subprocess.PIPE, stdout=archivo_backup)


def create_lock_file():
    bloquear = subprocess.run(["/usr/bin/touch", os.path.join(THIS_FOLDER, 'lock_db')])


def release_lock_file():
    delete_file(os.path.join(THIS_FOLDER, 'lock_db'))


def get_tables():
    obtener_tablas = subprocess.run(['mdb-tables', '-1', os.path.join(THIS_FOLDER, 'profe.mdb')],
                                    stdout=subprocess.PIPE)
    tablas = obtener_tablas.stdout
    lista_tablas = tablas.decode('ASCII').splitlines()
    lista_tablas.remove('TablaGrupos')
    return lista_tablas


def get_schema():
    obtener_schema = subprocess.run(['mdb-schema', os.path.join(THIS_FOLDER, 'profe.mdb'), 'mysql'],
                                    stdout=subprocess.PIPE)
    schema = obtener_schema.stdout
    return schema


def update_db():
    try:
        lista_tablas = get_tables()
        with open(os.path.join(THIS_FOLDER, 'dump.sql'), "a") as file_object:
            for i in lista_tablas:
                file_object.write("\n")
                file_object.write(f"DROP TABLE IF EXISTS {i};")
        schema = get_schema()
        with open(os.path.join(THIS_FOLDER, 'dump.sql'), "a") as file_object:
            file_object.write("\n")
            file_object.write(schema.decode('UTF-8'))
        for i in lista_tablas:
            comando = ['mdb-export', '-D', '%Y-%m-%d %H:%M:%S', '-I', 'mysql', os.path.join(THIS_FOLDER, "profe.mdb"), i]
            with open(os.path.join(THIS_FOLDER, 'dump.sql'), 'a') as file_object:
                proc = subprocess.call(comando, stderr=subprocess.PIPE, stdout=file_object)
        replaceAll(os.path.join(THIS_FOLDER, 'dump.sql'), 'IF NOT EXISTS IF NOT EXISTS', 'IF NOT EXISTS')
        replaceAll(os.path.join(THIS_FOLDER, 'dump.sql'), 'CREATE TABLE `TablaGrupos`', 'CREATE TABLE IF NOT EXISTS `TablaGrupos`')
        with open(os.path.join(THIS_FOLDER, 'dump.sql'), 'r') as f:
            command = ['mysql', 'pfisnqn$profe', '-h', 'pfisnqn.mysql.pythonanywhere-services.com', '-u', DBUSER, f'-p{DBPASS}']
            proc = subprocess.Popen(command, stdin=f)
            stdout, stderr = proc.communicate()
        command = ['mysql', '-sN', 'pfisnqn$profe', '-h', 'pfisnqn.mysql.pythonanywhere-services.com', '-u', DBUSER, f'-p{DBPASS}', '-e', 'select count(*) from PROFE;']
        with open(os.path.join(THIS_FOLDER, 'static', 'fecha'), 'w') as archivo_fecha:
            proc = subprocess.call(command, stderr=subprocess.PIPE, stdout=archivo_fecha)
        delete_file(os.path.join(THIS_FOLDER, 'profe.mdb'))
        delete_file(os.path.join(THIS_FOLDER, 'dump.sql'))
        print("Padrón actualizado")
    except subprocess.CalledProcessError as e:
        print(e.output)


def actualizar(pfis_file=False):
    if not pfis_file:
        id = get_file_id('pfis.rar')
        if id:
            download_file(id, os.path.join(THIS_FOLDER, 'pfis.rar'))
            time.sleep(5)
            if not os.path.isfile(os.path.join(THIS_FOLDER, 'pfis.rar')):
                print("error al descargar")
                return False
    if comparar_hashes():
        create_lock_file()
        backup_telefonos()
        extract_rar(os.path.join(THIS_FOLDER, 'pfis.rar'), THIS_FOLDER)
        delete_file(os.path.join(THIS_FOLDER, 'pfis_actual.rar'))
        rename_file(os.path.join(THIS_FOLDER, 'pfis.rar'), os.path.join(THIS_FOLDER, 'pfis_actual.rar'))
        update_db()
        release_lock_file()
        if pfis_file:
            return True
    else:
        if pfis_file:
            return False


def comparar_hashes():
    hash_nuevo = get_md5_hash(os.path.join(THIS_FOLDER, 'pfis.rar'))
    hash_actual = get_md5_hash(os.path.join(THIS_FOLDER, 'pfis_actual.rar'))
    if hash_nuevo == hash_actual:
        delete_file(os.path.join(THIS_FOLDER, 'pfis.rar'))
        print("Ya esta actualizado")
        return False
    else:
        print("Actualizando base de datos...")
        return True


def create_service():
    creds = get_credentials()
    try:
        service = build('drive', 'v3', credentials=creds)
        print('Drive service created')
    except HttpError as error:
        print(f'An error occurred: {error}')
    return service


def get_file_id(filename):
    results = service.files().list(includeItemsFromAllDrives = False,
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
        return None
    id = None
    for item in items:
        if item['name'] == filename:
            id = item['id']
    return id


def download_file(file_id, dest_filename):
    try:
        request = service.files().get_media(fileId=file_id)
        file = io.FileIO(dest_filename, 'wb')
        downloader = MediaIoBaseDownload(file, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print(f'Download {int(status.progress() * 100)}.')
    except HttpError as error:
        print(f'An error occurred: {error}')
        file = None
    except Exception as e:
        print(e)
        file = None
    return file


service = create_service()

if __name__ == '__main__':
    actualizar()
    sys.exit(0)