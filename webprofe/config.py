import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    if not os.environ.get('SQLALCHEMY_DATABASE_URI'):
        PG_USER = os.environ.get('PG_USER')
        PG_PASS = os.environ.get('PG_PASS')
        PG_HOST = os.environ.get('PG_HOST')
        PG_PORT = os.environ.get('PG_PORT')
        PG_NAME = os.environ.get('PG_NAME')
        # SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
        SQLALCHEMY_DATABASE_URI = 'postgres://{}:{}@{}:{}/{}?options=-c+timezone%3Dutc'.format(
            PG_USER, PG_PASS, PG_HOST, PG_PORT, PG_NAME
        )
        SQLALCHEMY_CONNECT_ARGS = {"options": "-c timezone=utc"}
    else:
        SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SQLALCHEMY_POOL_RECYCLE = 300
    # SQLALCHEMY_POOL_TIMEOUT = 20 # PROBAR COMO ALTERNATIVA A LAS OPTIONE = 299 # PROBAR COMO ALTS
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_recycle': 280, # Tiene que ser menor que el wait_timeout de mysql (28800 segundos) y menos que el POOL TIMEOUT
        'pool_timeout': 20, # Tiene que ser mayor que POOL_RECYCLE
        # 'pool_size': 10,
        # 'pool_pre_ping':True
    }
    TOKEN = os.environ.get('TOKEN')
    # net_read_timeout, net_write_timeout, wait_timeout

    RECAPTCHA3_PUBLIC_KEY = os.environ.get('RECAPTCHA_SITE_KEY')
    RECAPTCHA3_PRIVATE_KEY = os.environ.get('RECAPTCHA_SECRET_KEY')
    SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY')

    CADUCIDAD_CERT = 3650

    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True