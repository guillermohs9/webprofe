from flask import current_app, render_template, request, Blueprint, url_for, redirect, flash, session, Response, send_file, abort, jsonify
from webprofe import db, excel
from webprofe.models import Profe, Bajas, Dependencia, Solicitud, ARG, Telefono, xstr, User, Anotacion
from webprofe.main.forms import ElegirLocalidad, SolicitarLentes
from fpdf import FPDF
from flask_login import login_required, current_user
from functools import wraps
from datetime import datetime, timezone
from io import BytesIO
import zipfile
import os
from pathlib import Path
import re
import qrcode
import zlib
from base64 import urlsafe_b64encode as b64e, urlsafe_b64decode as b64d
from werkzeug.utils import secure_filename
import secrets
from werkzeug.wsgi import FileWrapper
import subprocess
from sys import platform
from sqlalchemy.sql import text
from webprofe.users.signer import sign_pdf
from webprofe.users.utils import solo_admins
from webprofe import extract


main = Blueprint('main', __name__)
chars = 3  # minimo de caracteres a buscar 


def actualizando(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if os.path.exists(os.path.join(current_app.root_path, 'lock_db')):
            return render_template('inicio_progreso.html')
        else:
            return f(*args, **kwargs)
    return decorated_function


def buscar(query, param=None):
    # connect to db
    try:
        engine = db.engine
        connection = engine.connect()
    except:
        print("Error")
        return
    # run a query and get the results
    s = text(query)
    results = connection.execute(s, {"x": param}).fetchall()
    result = results
    connection.close()
    return result


def upload_scan(archivo, nombre):
    sfilename = secure_filename(archivo.filename)
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(sfilename)
    filename = nombre + '_' + random_hex + f_ext
    archivo.save(os.path.join(current_app.root_path, 'static/uploads', filename))
    return filename


def agregar_telefono_paciente(paciente, num_tel):
    num_tel = re.sub("[^+0-9]", "", num_tel)
    buscar_tel = Telefono.query \
        .filter(Telefono.Numero_tel == num_tel) \
            .filter(Telefono.PROFE_Numero_Doc == paciente.Numero_Doc) \
                .first()
    if buscar_tel:
        return False
    tel = Telefono(PROFE_Numero_Doc=paciente.Numero_Doc,
                Numero_tel=num_tel)
    db.session.add(tel)
    db.session.commit()
    return True


@main.route("/solicitud_anteojos", methods=['GET', 'POST'])
@actualizando
@solo_admins
@login_required
def crear_solicitud_anteojos():
    form = SolicitarLentes()
    form.dependencia.choices.extend([(x.id, x.nombre) for x in Dependencia.query.order_by(Dependencia.nombre.asc()).all()])
    if form.validate_on_submit():
        buscar = Solicitud.query.filter(Solicitud.numero_ps == form.numero_ps.data.strip()).first()
        if buscar:
            flash("<span class='fs-5'>Ya hay un pedido cargado con ese numero de PS. Ref.: {str(buscar.id)}</span>", "danger")
            return redirect(request.url)
        solicitud = Solicitud(numero_ps=form.numero_ps.data.strip(),
                            numero_doc=form.numero_doc.data.strip(),
                            dependencia_id=form.dependencia.data)
        paciente = Profe.query.filter(Profe.Numero_Doc == form.numero_doc.data).first()
        if request.form['telefono_real_1']:
            if not agregar_telefono_paciente(paciente, request.form['telefono_real_1']):
                flash(f'<span class="fs-5">El número ya está cargado</span>', "warning")
        if request.form['telefono_real_2']:
            if not agregar_telefono_paciente(paciente, request.form['telefono_real_2']):
                flash(f'<span class="fs-5">El número ya está cargado</span>', "warning")
        nombre_paciente = re.sub(r'[\\/*?:"<>|]', "", paciente.ApeNom)
        nombre_paciente = nombre_paciente.strip().replace(" ", "_")
        receta = upload_scan(form.receta.data, f"{str(paciente.Numero_Doc)}_{nombre_paciente}")
        solicitud.receta = receta
        solicitud.nombre_paciente = paciente.ApeNom.strip()
        db.session.add(solicitud)
        db.session.commit()
        flash(f"<span class='fs-5'>Se registró la solicitud con éxito. Número de referencia: {str(solicitud.id)}</span>", "success")
        return redirect(url_for('main.crear_solicitud_anteojos'))
    return render_template('crear_solicitud.html', form=form, title="Solicitar lentes")


@main.route("/extracto_zip", methods=['POST'])
@actualizando
@solo_admins
@login_required
def zipped_data():
    if request.method == 'POST':
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        rootpath = Path(THIS_FOLDER)
        dir_uploads = os.path.join(rootpath.parent.absolute(), 'static', 'uploads')
        fileName = f"recetas_{datetime.utcnow().strftime('%d-%m-%Y')}.zip"
        memory_file = BytesIO()
        lista_ids = [int(x) for x in request.form.getlist('chk-extracto')]
        with zipfile.ZipFile(memory_file, 'w', zipfile.ZIP_DEFLATED) as zipf:
            for i in lista_ids:
                sol = Solicitud.query.get_or_404(i)
                ruta_receta = os.path.join(dir_uploads, sol.receta)
                zipf.write(ruta_receta, os.path.basename(ruta_receta))
        memory_file.seek(0)
        w = FileWrapper(memory_file)
        resp = Response(w, mimetype="application/zip", direct_passthrough=True)
        resp.headers['Content-Disposition'] = f'attachment; filename="{fileName}"'
        return resp
        # return send_file(memory_file,
        #                 attachment_filename=fileName,
        #                 as_attachment=True)


@main.route("/extracto_solic", methods=['POST'])
@actualizando
@solo_admins
@login_required
def extracto_solic():
    if request.method == 'POST':
        lista_ids = [int(x) for x in request.form.getlist('chk-extracto')]
        ret = [['N° PS', 'Fecha', 'DNI', 'Nombre', 'Edad', 'Tipo pensión', 'Tel.', 'Efector']]
        for i in lista_ids:
            sol = Solicitud.query.get_or_404(i)
            ret.append([sol.numero_ps,
                        sol.fecha_printable(),
                        sol.numero_doc,
                        sol.nombre_paciente,
                        sol.edad(),
                        sol.ley_aplicada(),
                        sol.telefonos_paciente(),
                        sol.dependencia.nombre])
        return excel.make_response_from_array(ret, "xlsx", file_name=f"extracto_{datetime.utcnow().strftime('%d-%m-%Y')}")


@main.route("/gestionar", methods=['POST'])
@actualizando
@solo_admins
@login_required
def gestionar():
    if request.method == 'POST':
        lista_ids = [int(x) for x in request.form.getlist('chk-extracto')]
        for i in lista_ids:
            sol = Solicitud.query.get_or_404(i)
            sol.gestionada = True
        db.session.commit()
        return redirect(url_for('main.ver_solicitudes'))
    

@main.route("/ver_solicitud", methods=['POST'])
@actualizando
@login_required
def ver_solicitud_anteojos():
    if request.method == 'POST':
        sol_id = request.form['solicitud_id']
        sol = Solicitud.query.get_or_404(sol_id)
        return render_template('solicitud_anteojos.html', sol=sol, title=f"Solicitud lentes N° {sol.id}")

    
@main.route("/eliminar_gestionadas", methods=['POST'])
@actualizando
@solo_admins
@login_required
def eliminar_gestionadas():
    if request.method == 'POST':
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        rootpath = Path(THIS_FOLDER)
        dir_uploads = os.path.join(rootpath.parent.absolute(), 'static', 'uploads')
        solicitudes = Solicitud.query \
            .filter(Solicitud.gestionada.is_(True)) \
                .filter(Solicitud.receta.isnot(None)) \
                    .all()
        cant = len(solicitudes)
        counter = 0
        if solicitudes:
            for i in solicitudes:
                ruta_receta = os.path.join(dir_uploads, i.receta)
                try:
                    os.remove(ruta_receta)
                    counter += 1
                except OSError:
                    pass
                i.receta = None
            db.session.commit()
            flash(f"<span class='fs-5'>Se borraron {counter} de {cant} archivos</span>", "warning")
        else:
            flash(f"<span class='fs-5'>No hay archivos para borrar</span>", "warning")
        return redirect(url_for('main.ver_solicitudes'))


@main.route("/listado_solicitudes", methods=['GET'])
@actualizando
@solo_admins
@login_required
def ver_solicitudes():
    porcentaje = None
    if platform == "linux":
        disponible = 512.0
        if os.environ.get('PYTHONANYWHERE_DOMAIN'):
            output = subprocess.getoutput('du -cs /tmp ~ | awk "/total/ {print $1}"')
            usado = int(output.split()[0]) / 1024
        else:
            output = subprocess.getoutput("du -cs /home/guillermo/Descargas | awk '{print $1}'")
            usado = int(output.split()[0]) / 1024
        porcentaje = round((usado / disponible) * 100)
    else:
        porcentaje = 50
    solicitudes = Solicitud.query.order_by(Solicitud.fecha.asc()).all()
    solicitudes_pendientes = Solicitud.query.filter(Solicitud.gestionada.is_(False)).count()
    return render_template('listado_anteojos.html', solicitudes=solicitudes, 
                           title="Solicitudes de anteojos", porcentaje=porcentaje,
                           solicitudes_pendientes=solicitudes_pendientes)


@main.route("/purgar_solicitudes", methods=['GET'])
@actualizando
@solo_admins
@login_required
def purgar_solicitudes():
    solicitudes = Solicitud.query.all()
    for s in solicitudes:
        pac = Profe.query.filter(s.numero_doc == Profe.Numero_Doc).first()
        if not pac:
            print(f'Eliminando {s}')
            db.session.delete(s)
    db.session.commit()
    flash(f"<span class='fs-5'>Se purgó la lista de solicitudes</span>", "danger")
    return redirect(url_for('main.inicio'))


@main.route("/purgar_usuarios", methods=['GET'])
@actualizando
@solo_admins
@login_required
def purgar_usuarios():
    usuarios = User.query.all()
    for u in usuarios:
        pac_a = Profe.query.filter(u.dni == Profe.Numero_Doc).first()
        if pac_a:
            if not u.baja:
                u.baja = True
                flash(f"<span class='fs-5'>Dado de baja DNI {str(u.dni)}</span>", "success")
    db.session.commit()
    flash(f"<span class='fs-5'>Se purgó la lista de usuarios</span>", "danger")
    return redirect(url_for('main.inicio'))



@main.route("/", methods=['GET', 'POST'])
@actualizando
@login_required
def inicio():
    form = ElegirLocalidad()
    anotaciones = Anotacion.query.order_by(Anotacion.fecha.desc()).limit(5).all()
    form.localidad.choices = [(f"{x.Departamento}-{x.Localidad}", f"{x.Nombre_Loc.title()}") for x in
                              buscar("select * from LOCALIDAD where Provincia = 15 ORDER BY Nombre_Loc ASC")]
    if form.validate_on_submit():
        codigo = form.localidad.data.split('-')
        results = Profe.query \
            .filter_by(Cug_Pcia=15) \
                .filter_by(Cug_Dpto=codigo[0]) \
                    .filter_by(Cug_loc=codigo[1]) \
                        .all()
        if len(results) > 0:
            form = SolicitarLentes()
            return render_template('resultados.html', results=results, form=form)
        else:
            flash(f"<span class='fs-5'>No hay resultados en esa localidad</span>", "danger")
            return redirect(url_for('main.inicio'))
    return render_template('inicio.html', form=form, anotaciones=anotaciones)


@main.route("/duplicados", methods=['GET'])
@actualizando
@login_required
def duplicados():
    duplicados = buscar("SELECT Numero_Doc FROM PROFE GROUP BY Numero_Doc HAVING COUNT(Numero_Doc) > 1")
    if duplicados:
        lines = "\n".join([str(i.Numero_Doc) for i in duplicados])
        flash(f"""
        <div><h4 class="alert-heading">Se encontraron {str(len(duplicados))} duplicados</h4></div>
          <div class="d-flex justify-content-center">
            <div class="p-5">
              <form style="margin: 0; padding: 0;" target=_blank action="/multi" method="post">
                <input type="hidden" name="textarea" value="{lines}"/>
                <input class="btn btn-secondary border shadow btn-sm" href=# value="Ver detalle" type="submit"/>
              </form>
          </div>
        </div>
        """, "secondary")
    else:
        flash(f"<span class='fs-5'>No hay registros duplicados</span>", "success")
    return redirect(url_for('main.inicio'))


@main.route("/resultados_bajas", methods=['GET'])
@actualizando
@login_required
def resultados_bajas():
    results = session.pop('results', None)
    return render_template('resultados_bajas.html', resultados=results)


@main.route("/altas/dni", methods=['POST'])
@actualizando
@login_required
def buscar_altas_dni():
    message = ''
    if request.method == 'POST':
        dni = request.form.get('dni')
        if dni == 0 or dni is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        results = Profe.query.filter(Profe.Numero_Doc == dni).all()
        if len(results) > 0:
            form = SolicitarLentes()
            if len(results) == 1 and not results[0].numeros_telefonicos_obj():
                flash(f'<span class="fs-5">Por favor agregar teléfono de contacto si es posible</span>', "warning")
            return render_template('resultados.html', results=results, busqueda=str(dni), form=form, lentes=len(results) == 1)
        else:
            flash(f"""
            <span class="fs-5">No hay resultados con DNI N° {str(dni)}</span>
            <div class="d-flex justify-content-center">
              <div class="p-2">
                <a class="btn btn-primary" target="_blank" href="https://egov.andis.gob.ar/pfis/consulta_beneficiario_cp.asp?NumeroDocPFIS={str(dni)}"><i class="fa fa-search"></i> Buscar en web ANDIS</a>
              </div>
              <div class="p-2">
                <form style="margin: 0; padding: 0;" target=_blank action="/negativa_dni" method="post">
                  <input type="hidden" name="dni" value="{dni}"/>
                  <button class="btn btn-info" role="button" type="submit"><i class="fas fa-times-circle"></i> Constancia negativa</button>
                </form>
              </div>
              <div class="p-2">
                <form style="margin: 0; padding: 0;" target=_blank action="/bajas/dni" method="post">
                  <input type="hidden" name="dni" value="{dni}"/>
                  <button class="btn btn-danger" type="submit" role="button"><i class="fa fa-search"></i> Buscar en Bajas</button>
                </form>
              </div>
            </div>
            """, "danger")
            return redirect(url_for('main.inicio'))
        

@main.route("/anotaciones", methods=['GET'])
@actualizando
@solo_admins
@login_required
def ver_anotaciones():
    anotaciones = Anotacion.query.order_by(Anotacion.fecha.desc()).all()
    return render_template('anotaciones.html', anotaciones=anotaciones)


@main.route("/purgar_anotaciones", methods=['GET'])
@actualizando
@solo_admins
@login_required
def purgar_anotaciones():
    anotaciones = Anotacion.query.all()
    for i in anotaciones:
        if not i.obtener_paciente():
            db.session.delete(i)
    db.session.commit()
    flash("<span class='fs-5'>Se purgaron las anotaciones</span>", "success")
    return render_template('anotaciones.html', anotaciones=anotaciones)


@main.route("/anotacion", methods=['POST'])
@actualizando
@solo_admins
@login_required
def anotacion():
    if request.method == 'POST':
        dni = int(request.form['dni'])
        paciente = Profe.query.filter(Profe.Numero_Doc == int(dni)).first()
        if paciente:
            texto = request.form['texto'].strip()
            if texto != "":
                anotacion = Anotacion(PROFE_Numero_Doc=dni,
                                      texto=texto,
                                      user_id=current_user.id)
                db.session.add(anotacion)
                db.session.commit()
                flash(f'<span class="fs-5">Se agregó la anotacion al paciente {paciente.ApeNom.title()}, DNI {paciente.Numero_Doc}</span>', "success")
                return render_template('resultados.html', results=[paciente])
            else:
                flash(f'<span class="fs-5">No se ingresó anotación</span>', "warning")
                return redirect(url_for('main.inicio'))
        else:
            flash(f'<span class="fs-5">Error</span>', "danger")
            return redirect(url_for('main.inicio'))
        

@main.route('/eliminar_anotacion', methods=['POST'])
@actualizando
@solo_admins
@login_required
def eliminar_anotacion():
    if request.method == 'POST':
        anotacion_id = int(request.form['anotacion_id'])
        anotacion = Anotacion.query.get_or_404(anotacion_id)
        results = Profe.query.filter(Profe.Numero_Doc == anotacion.PROFE_Numero_Doc).all()
        db.session.delete(anotacion)
        db.session.commit()
        flash("<span class='fs-5'>Se eliminó la anotación</span>", "danger")
        if 'viene_de_lista' in request.form:
            anotaciones = Anotacion.query.order_by(Anotacion.fecha.desc()).all()
            return render_template('anotaciones.html', anotaciones=anotaciones)
        else:
            return render_template('resultados.html', results=results)
        

@main.route("/buscar/anotaciones", methods=['POST'])
@actualizando
@solo_admins
@login_required
def buscar_anotaciones():
    if request.method == 'POST':
        anotacion = request.form.get('anotacion').strip()
        anotacion = anotacion.replace(" ", "%")
        if anotacion == "" or anotacion is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        search = f"%{anotacion}%"
        anotaciones = Anotacion.query.filter(Anotacion.texto.like(search)).all()
        if anotaciones:
            lista_dnis = [i.PROFE_Numero_Doc for i in anotaciones]
            drop_tabla = db.session.execute(text('DROP TABLE IF EXISTS ConsultaMulti;'))
            crear = db.session.execute(text('CREATE TEMPORARY TABLE ConsultaMulti ( dni int(11) NOT NULL );'))

            insert_text = "INSERT INTO ConsultaMulti (dni) VALUES "
            for i in lista_dnis:
                insert_text += f"({i}),"
            insert_text = insert_text[:-1]
            insert_text += ";"
            insertar = db.session.execute(text(insert_text))
            results = Profe.query.filter(Profe.Numero_Doc.in_(lista_dnis)).all()
            result_neg = db.session.execute(
                text('SELECT ConsultaMulti.dni FROM ConsultaMulti LEFT JOIN PROFE p ON ConsultaMulti.dni = p.Numero_Doc WHERE p.Numero_Doc IS NULL;'))
            filas = result_neg.fetchall()
            if len(filas) == 0:
                filas = None
            if results:
                drop_tabla = db.session.execute(text('DROP TABLE ConsultaMulti;'))
                form = SolicitarLentes()
                return render_template('resultados.html', results=results, result_neg=filas, form=form)
            else:
                flash(f'<span class="fs-5">No hay resultados para "{anotacion}"</span>', "danger")
                return redirect(url_for('main.inicio'))
        else:
            flash(f'<span class="fs-5">No hay resultados para "{anotacion}"</span>', "danger")
            return redirect(url_for('main.inicio'))
        

@main.route("/actualizar", methods=['POST'])
@actualizando
@solo_admins
@login_required
def actualizar():
    if request.method == 'POST':
        if 'archivo_pfis' not in request.files:
            flash(f'<span class="fs-5">No hay archivo</span>', "danger")
            return redirect(url_for('main.inicio'))
        file = request.files['archivo_pfis']
        if file.filename == '' or file.filename != "pfis.rar":
            flash(f'<span class="fs-5">Error en el archivo presentado</span>', "danger")
            return redirect(url_for('main.inicio'))
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(current_app.root_path, filename))
            if extract.actualizar(pfis_file=True):
                flash(f'<span class="fs-5">Se actualizó el padrón</span>', "success")
            else:
                flash(f'<span class="fs-5">El padrón ya está actualizado</span>', "warning")
            return redirect(url_for('main.inicio'))


@main.route("/api/dni/<int:dni>")
@actualizando
def api_dni(dni):
    token = request.headers.get('X-Custom-Token')
    if token != current_app.config['TOKEN']:
        return jsonify({'error': 'Unauthorized access'}), 401
    resultado = Profe.query.filter(Profe.Numero_Doc == dni).first()
    return resultado.to_dict()


@main.route("/altas/buscar/dni/<int:dni>")
@actualizando
@login_required
def chequear_alta_dni(dni):
    resultado = Profe.query.filter(Profe.Numero_Doc == dni).first()
    solicitudes = Solicitud.query.filter(Solicitud.numero_doc == dni).all()
    dic = {}
    if resultado:
        if resultado.numeros_telefonicos():
            dic['telefonos'] = []
            for i in resultado.numeros_telefonicos():
                dic['telefonos'].append(i)
        if solicitudes:
            dic['solicitudes'] = {}
            for i in solicitudes:
                dic['solicitudes'][i.id] = {'numero_ps' : i.numero_ps,
                                            'id' : i.id,
                                            'fecha' : i.fecha.strftime("%d/%m/%Y"),
                                            'gestionada' : i.gestionada}
        dic['nombre'] = resultado.ApeNom.strip().replace(",", " ")
        dic['dni'] = resultado.Numero_Doc
    return dic


@main.route("/altas/nom", methods=['POST'])
@actualizando
@login_required
def buscar_altas_nom():
    if request.method == 'POST':
        nom = request.form.get('nom').strip()
        nom = nom.replace(" ", "%")
        if nom == "" or nom is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        if len(nom) < chars:
            flash(f"<span class='fs-5'>Ingrese al menos {str(chars)} caracteres para la búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        search = f"%{nom}%"
        results = Profe.query.filter(Profe.ApeNom.like(search)).all()
        if results:
            form = SolicitarLentes()
            return render_template('resultados.html', results=results, busqueda=nom, form=form, lentes=len(results) == 1)
        else:
            flash(f'<span class="fs-5">No hay resultados para "{nom}"</span>', "danger")
            return redirect(url_for('main.inicio'))
        

@main.route("/altas/tel", methods=['POST'])
@actualizando
@login_required
def buscar_altas_tel():
    if request.method == 'POST':
        tel = request.form.get('telefono_real').strip()
        if tel == "" or tel is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        results = Profe.query \
            .join(Telefono) \
                .filter(Telefono.PROFE_Numero_Doc == Profe.Numero_Doc) \
                    .filter(Telefono.Numero_tel == tel) \
                        .all()
        if results:
            form = SolicitarLentes()
            return render_template('resultados.html', results=results, form=form)
        else:
            flash(f'<span class="fs-5">No hay resultados para "{tel}"</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route("/altas/recientes", methods=['POST'])
@actualizando
@login_required
def altas_recientes():
    if request.method == 'POST':
        fecha = request.form.get('fecha')
        if fecha == "" or fecha is None:
            flash("<span class='fs-5'>No se ingresó fecha</span>", "danger")
            return redirect(url_for('main.inicio'))
        results = Profe.query.filter(Profe.Fech_Alta >= fecha).all()
        if len(results) > 0:
            form = SolicitarLentes()
            return render_template('resultados.html', results=results, busqueda=fecha, altas=True, form=form)
        else:
            flash(f'<span class="fs-5">No hay altas en el período seleccionado</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route("/bajas/recientes", methods=['POST'])
@actualizando
@login_required
def bajas_recientes():
    if request.method == 'POST':
        fecha = request.form.get('fecha')
        if fecha == "" or fecha is None:
            flash("<span class='fs-5'>No se ingresó fecha</span>", "danger")
            return redirect(url_for('main.inicio'))
        results = Bajas.query.filter(Bajas.fechbaja >= fecha).all()
        if len(results) > 0:
            return render_template('resultados_bajas.html', results=results, busqueda=fecha, bajas=True)
        else:
            flash(f'<span class="fs-5">No hay bajas en el período seleccionado</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route("/bajas/dni", methods=['POST'])
@actualizando
@login_required
def buscar_bajas_dni():
    if request.method == 'POST':
        dni = request.form.get('dni')
        if dni == 0 or dni is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        results = Bajas.query.filter(Bajas.numero_doc == dni).all()
        if len(results) > 0:
            return render_template('resultados_bajas.html', results=results, busqueda=str(dni))
        else:
            flash(f'<span class="fs-5">No hay resultados con DNI N° {str(dni)} en BAJAS</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route("/bajas/nom", methods=['POST'])
@actualizando
@login_required
def buscar_bajas_nom():
    if request.method == 'POST':
        nom = request.form.get('nom').strip()
        nom = nom.replace(" ", "%")
        if nom == "" or nom is None:
            flash("<span class='fs-5'>No se ingresó búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        if len(nom) < chars:
            flash(f"<span class='fs-5'>Ingrese al menos {str(chars)} caracteres para la búsqueda</span>", "danger")
            return redirect(url_for('main.inicio'))
        search = f"%{nom}%"
        results = Bajas.query.filter(Bajas.apenom.like(search)).all()
        if len(results) > 0:
            return render_template('resultados_bajas.html', results=results, busqueda=nom)
        else:
            flash(f'<span class="fs-5">No hay resultados para "{nom}" en BAJAS</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route("/negativa_dni", methods=['POST'])
@actualizando
@login_required
def negativa_dni():
    if request.method == 'POST':
        dni = request.form.get('dni')
        if dni == "" or dni is None:
            flash("<span class='fs-5'>No se recibieron datos</span>", "danger")
            return redirect(url_for('main.inicio'))
        pdf = FPDF()
        pdf.set_left_margin(20)
        pdf.set_right_margin(20)
        pdf.set_top_margin(15)
        pdf.add_page()
        pdf.set_font("Arial", size=8)
        pdf.cell(0, 25, '', 0, 1, 'C')
        pdf.cell(0, 15, "Fecha y hora de impresión: " + datetime.now().replace(tzinfo=timezone.utc).astimezone(ARG).strftime("%d/%m/%Y %H:%M"), 0, 1, 'R')
        pdf.image(os.path.join(current_app.root_path, 'static', 'data'), x=18, y=12, w=175, h=25, type="PNG")
        pdf.set_font("Arial", "B", size=18)
        pdf.cell(0, 5, '', 0, 1, 'C')
        pdf.cell(0, 15, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.set_font("Arial", size=12)
        pdf.cell(0, 6, 'Se deja constancia que no se registran Beneficiarios del Programa Incluir Salud', 0, 1, 'L')
        pdf.cell(0, 6, f'con el N° DNI {dni} empadronados al día de la fecha en la provincia de Neuquén.', 0, 1, 'L')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.set_font("Arial", size=7)
        pdf.cell(0, 10, '* Información suministrada por el Padrón de Beneficiarios del Programa Incluir Salud', 0, 1,
                 'C')
        nombre_archivo_constancia = f"negativa_{dni}"
        byte_string = pdf.output(dest='S').encode('latin-1') 
        stream = BytesIO(byte_string) 
        sign_pdf(current_user, stream)
        return send_file(stream,
                mimetype = "application/pdf",
                download_name = secure_filename(f"{nombre_archivo_constancia}.pdf"))


def obscure(str) -> bytes:
    return b64e(zlib.compress(str, 9))


def unobscure(obscured: bytes) -> bytes:
    return zlib.decompress(b64d(obscured))


def generar_constancia_alta(dni, hash, verifica=False):
    afiliado = buscar(
    "select PROFE.ApeNom, PROFE.Tipo_Doc, PROFE.Numero_Doc, PROFE.Fe_Nac, PROFE.Clave_ExCaja, PROFE.Clave_Tipo, "
    "PROFE.Clave_Numero, PROFE.Clave_Coparticipe, PROFE.Clave_Parentesco, PROFE.Fech_Alta, PROFE.Dom_Calle, "
    "PROFE.Dom_Nro, PROFE.Dom_Piso, PROFE.Dom_Dpto, LOCALIDAD.Nombre_Loc, PROFE.Cod_Pos, "
    "DEPARTAMENTO.Nombre_Dep, TablaGrupos.Descripcion from "
    "PROFE, DEPARTAMENTO, LOCALIDAD, TablaGrupos WHERE PROFE.LeyAplicada=TablaGrupos.Codigo AND PROFE.Cug_Dpto=DEPARTAMENTO.Departamento AND "
    "PROFE.Cug_Pcia=DEPARTAMENTO.Provincia AND PROFE.Cug_Pcia=LOCALIDAD.Provincia AND "
    "PROFE.Cug_Dpto=LOCALIDAD.Departamento AND PROFE.Cug_loc=LOCALIDAD.Localidad AND PROFE.Numero_Doc = :x",
    dni
    )[0]

    url_verif = f"https://pfisnqn.pythonanywhere.com/verificar/{hash}"
    qr = qrcode.QRCode(
        version=4,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=15,
        border=4,
    )
    qr.add_data(url_verif)
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")
    filename = f"{secrets.token_hex(8)}.png"
    img.save(filename)
    try:
        if len(afiliado) > 0:
            data = [["Nombre y Apellido", xstr(afiliado.ApeNom).strip()],
                    ["N° Doc (" + afiliado.Tipo_Doc + ")", xstr(afiliado.Numero_Doc)],
                    # ["Fecha de Nacimiento", afiliado.Fe_Nac], # 
                    ["Fecha de Nacimiento", afiliado.Fe_Nac.strftime("%d/%m/%Y")], # 
                    ["Beneficio N°",
                        xstr(afiliado.Clave_ExCaja) + xstr(afiliado.Clave_Tipo) + xstr(afiliado.Clave_Numero) + xstr(
                            afiliado.Clave_Coparticipe) + xstr(afiliado.Clave_Parentesco)],
                    ["Ley Aplicada", afiliado.Descripcion],
                    # ["Fecha de Alta", afiliado.Fech_Alta], # 
                    ["Fecha de Alta", afiliado.Fech_Alta.strftime("%d/%m/%Y")], # 
                    ["Domicilio", xstr(afiliado.Dom_Calle).strip() + " " + xstr(afiliado.Dom_Nro).strip()],
                    ["", "Piso: " + xstr(afiliado.Dom_Piso).strip() + " Dpto: " + xstr(afiliado.Dom_Dpto).strip()],
                    ["Localidad", xstr(afiliado.Nombre_Loc)],
                    ["Código postal", xstr(afiliado.Cod_Pos).strip()],
                    ["Departamento", xstr(afiliado.Nombre_Dep)]
                    ]

            pdf = FPDF()
            pdf.set_left_margin(20)
            pdf.set_right_margin(20)
            pdf.set_top_margin(15)
            pdf.add_page()
            pdf.set_font("Arial", size=8)
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.cell(0, 15, "Fecha y hora de impresión: " + datetime.now().replace(tzinfo=timezone.utc).astimezone(ARG).strftime("%d/%m/%Y %H:%M"), 0, 1, 'R')
            pdf.image(os.path.join(current_app.root_path, 'static', 'data'), x=18, y=12, w=175, h=25, type="PNG")
            pdf.set_font("Arial", "B", size=18)
            pdf.cell(0, 5, '', 0, 1, 'C')
            pdf.cell(0, 15, 'Constancia de Afiliación PFIS', 0, 1, 'C')
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.set_font("Arial", size=12)
            col_width = pdf.w / 4.5
            row_height = pdf.font_size
            for row in data:
                for item in row:
                    pdf.cell(col_width, row_height * 2, txt=item, border=0)
                pdf.ln(row_height * 2)
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.cell(0, 10, '', 0, 1, 'C')
            pdf.image(filename, x=20, y=185, w=30, h=30, type="PNG")
            pdf.set_font("Arial", size=6)
            # pdf.cell(0, 10, f'Código verificación: {hash}', 0, 1, 'L')
            pdf.cell(0, 5, '', 0, 1, 'C')
            pdf.set_font("Arial", size=7)
            pdf.cell(0, 10, '* Información suministrada por el Padrón de Beneficiarios del Programa Incluir Salud', 0, 1,
                        'L')
            nombre_archivo_constancia = secure_filename(f"Constancia_{afiliado.Numero_Doc}_{afiliado.ApeNom.strip().title().replace(' ', '_')}.pdf")
            byte_string = pdf.output(dest='S').encode('latin-1')  
            stream = BytesIO(byte_string) 
            if not verifica:
                sign_pdf(current_user, stream)
            return stream, nombre_archivo_constancia
    except Exception as e:
        print(e)
        flash(f'<span class="fs-5">Error al generar constancia PDF</span>', "danger")
        return None
    finally:
        try:
            os.remove(filename)
        except:
            pass


@main.route("/verificar/<hash>", methods=['GET'])
@actualizando
def constancia_verificacion(hash):
    try:
        dni = unobscure(hash.encode('utf-8'))
    except:
        flash("<span class='fs-5'>Error de validación de hash</span>", "danger")
        abort(403)
    archivo, nombre_archivo = generar_constancia_alta(dni, hash, verifica=True)
    if archivo:
        return send_file(archivo,
                mimetype = "application/pdf",
                download_name = nombre_archivo)
    else:
        abort(403)


@main.route("/constancia", methods=['POST'])
@actualizando
@login_required
def constancia():
    if request.method == 'POST':
        dni = request.form.get('dni')
        hash = obscure(dni.encode('utf-8')).decode('utf-8')
    archivo, nombre_archivo = generar_constancia_alta(dni, hash)
    if archivo:
        return send_file(archivo,
                mimetype = "application/pdf",
                download_name = nombre_archivo)
    else:
        return redirect(url_for('main.inicio'))


@main.route("/constancia_baja", methods=['POST'])
@actualizando
@login_required
def constancia_baja():
    if request.method == 'POST':
        dni = request.form.get('dni')
        afiliado = buscar(
            "select BAJAS.apenom, BAJAS.clave_excaja, BAJAS.clave_tipo, BAJAS.clave_numero, BAJAS.clave_coparticipe, "
            "BAJAS.clave_parentesco, BAJAS.tipo_doc, BAJAS.numero_doc, BAJAS.fe_nac, BAJAS.fechalta, BAJAS.fechbaja, "
            "MotivosBajas.Desc_Mov, LOCALIDAD.Nombre_Loc, TablaGrupos.Descripcion FROM BAJAS, MotivosBajas, LOCALIDAD, TablaGrupos WHERE "
            "BAJAS.motivo=MotivosBajas.Motivo AND BAJAS.leyaplicada=TablaGrupos.Codigo AND BAJAS.cug_pcia=LOCALIDAD.Provincia AND "
            "BAJAS.cug_dpto=LOCALIDAD.Departamento AND BAJAS.cug_loc=LOCALIDAD.Localidad AND BAJAS.numero_doc = :x",
            dni
        )[0]
        data = [["Nombre y Apellido", xstr(afiliado.apenom).strip()],
                ["N° Doc (" + xstr(afiliado.tipo_doc) + ")", xstr(afiliado.numero_doc)],
                ["Beneficio N°",
                 xstr(afiliado.clave_excaja) + xstr(afiliado.clave_tipo) + xstr(afiliado.clave_numero) + xstr(
                     afiliado.clave_coparticipe) + xstr(afiliado.clave_parentesco)],
                # ["Fecha de Nacimiento", afiliado.fe_nac],
                ["Fecha de Nacimiento", afiliado.fe_nac.strftime("%d/%m/%Y")],
                ["Ley Aplicada", afiliado.Descripcion],
                # ["Fecha de Alta", afiliado.fechalta],
                ["Fecha de Alta", afiliado.fechalta.strftime("%d/%m/%Y")],
                # ["Fecha de Baja", afiliado.fechbaja],
                ["Fecha de Baja", afiliado.fechbaja.strftime("%d/%m/%Y")],
                ["Motivo de Baja", xstr(afiliado.Desc_Mov)],
                ["Localidad", afiliado.Nombre_Loc]
                ]

        pdf = FPDF()
        pdf.set_left_margin(20)
        pdf.set_right_margin(20)
        pdf.set_top_margin(15)
        pdf.add_page()
        pdf.set_font("Arial", size=8)
        pdf.cell(0, 15, "Fecha y hora de impresión: " + datetime.now().replace(tzinfo=timezone.utc).astimezone(ARG).strftime("%d/%m/%Y %H:%M"), 0, 1, 'R')
        pdf.set_font("Arial", "B", size=18)
        pdf.cell(0, 5, '', 0, 1, 'C')
        pdf.set_text_color(255, 0, 0)
        pdf.cell(0, 15, 'Constancia de BAJA de padrón PFIS', 0, 1, 'C')
        pdf.set_text_color(0, 0, 0)
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.set_font("Arial", size=12)
        col_width = pdf.w / 4.5
        row_height = pdf.font_size
        for row in data:
            for item in row:
                pdf.cell(col_width, row_height * 2,
                         txt=item, border=0)
            pdf.ln(row_height * 2)
        pdf.cell(0, 10, '', 0, 1, 'C')
        pdf.set_font("Arial", size=7)
        pdf.cell(0, 10, '* Información suministrada por el Padrón de Beneficiarios del Programa Incluir Salud', 0, 1,
                 'C')
        nombre_archivo_constancia = afiliado.apenom.strip().title().replace(" ", "_")
        byte_string = pdf.output(dest='S').encode('latin-1') 
        stream = BytesIO(byte_string) 
        sign_pdf(current_user, stream)
        return send_file(stream,
                mimetype = "application/pdf",
                download_name = secure_filename(f"Baja_{afiliado.numero_doc}_{nombre_archivo_constancia}.pdf"))


@main.route('/multi', methods=['POST'])
@actualizando
@login_required
def multi():
    if request.method == 'POST':
        # datos = request.form['textarea']
        contents = request.form['textarea']
        lineas = contents.splitlines()
        lineas = [x for x in lineas if x]
        if len(lineas) == 0:
            flash("<span class='fs-5'>No hay datos</span>", "danger")
            return redirect(url_for('main.inicio'))
        lista_dnis = [re.sub("[^0-9]", "", i) for i in lineas]

        drop_tabla = db.session.execute(text('DROP TABLE IF EXISTS ConsultaMulti;'))
        crear = db.session.execute(text('CREATE TEMPORARY TABLE ConsultaMulti ( dni int(11) NOT NULL );'))

        insert_text = "INSERT INTO ConsultaMulti (dni) VALUES "
        for i in lista_dnis:
            insert_text += f"({i}),"
        insert_text = insert_text[:-1]
        insert_text += ";"
        insertar = db.session.execute(text(insert_text))
        results = Profe.query.filter(Profe.Numero_Doc.in_(lista_dnis)).all()
        result_neg = db.session.execute(
            text('SELECT ConsultaMulti.dni FROM ConsultaMulti LEFT JOIN PROFE p ON ConsultaMulti.dni = p.Numero_Doc WHERE p.Numero_Doc IS NULL;'))
        filas = result_neg.fetchall()
        if len(filas) == 0:
            filas = None
        if results:
            drop_tabla = db.session.execute(text('DROP TABLE ConsultaMulti;'))
            form = SolicitarLentes()
            return render_template('resultados.html', results=results, result_neg=filas, form=form)
        else:
            flash(f'<span class="fs-5">No hay resultados</span>', "danger")
            return redirect(url_for('main.inicio'))


@main.route('/agregar_telefono', methods=['POST'])
@actualizando
@login_required
def agregar_telefono():
    if request.method == 'POST':
        if request.form['numero_doc_form'] is None:
            flash("<span class='fs-5'>Ocurrió un error</span>", "danger")
            return redirect(url_for('main.inicio'))
        dni = request.form['numero_doc_form']
        paciente = Profe.query.filter(Profe.Numero_Doc == int(dni)).first()
        if not paciente:
            flash(f'<span class="fs-5">Ocurrió un error</span>', "danger")
            return redirect(url_for('main.inicio'))
        form = SolicitarLentes()
        if agregar_telefono_paciente(paciente, request.form['telefono_real']):
            flash(f'<span class="fs-5">Se agregó el teléfono correctamente</span>', "success")
        else:
            flash(f'<span class="fs-5">El número ya está cargado</span>', "warning")
        return render_template('resultados.html', results=[paciente], form=form)
    


# @main.route('/populate_telefono', methods=['GET'])
# @actualizando
# def populate_telefono():
#     telefonos = Telefonos.query.all()
#     for x in telefonos:
#         nro_doc = x.Numero_doc
#         lista_tels = json.loads(x.Numeros_tel)
#         for x in range(len(lista_tels)):
#             if not lista_tels[x].startswith('+54'):
#                 lista_tels[x] = f'+54{lista_tels[x]}'
#         for t in lista_tels:
#             tel = Telefono(PROFE_Numero_Doc=nro_doc,
#                             Numero_tel=t)
#             db.session.add(tel)
#     db.session.commit()
#     rol_admin = Rol(descripcion="Admin")
#     rol_user = Rol(descripcion="Usuario")
#     db.session.add(rol_admin)
#     db.session.add(rol_user)
#     db.session.commit()
#     return redirect(url_for('main.inicio'))


@main.route('/eliminar_telefono', methods=['POST'])
@actualizando
@login_required
def eliminar_telefono():
    if request.method == 'POST':
        telefono_id = int(request.form['tel_id'])
        telefono = Telefono.query.get_or_404(telefono_id)
        form = SolicitarLentes()
        results = Profe.query.filter(Profe.Numero_Doc == telefono.PROFE_Numero_Doc).all()
        db.session.delete(telefono)
        db.session.commit()
        flash("<span class='fs-5'>Se eliminó el teléfono</span>", "danger")
        return render_template('resultados.html', results=results, form=form)