from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileRequired
from wtforms import SelectField, ValidationError
from wtforms import StringField, SelectField, FileField
from wtforms.validators import DataRequired, Length, ValidationError



class myForm(FlaskForm):
    class Meta:
        csrf = False


class MySelectField(SelectField):
    def pre_validate(self, SolicitarLentes):
        if self.data == 0:
            raise ValidationError("Debe seleccionar efector")


class ElegirLocalidad(myForm):
    localidad = SelectField('Localidad', choices=[])


class SolicitarLentes(myForm):
    numero_doc = StringField('N° de Documento', validators=[DataRequired()])
    numero_ps = StringField('N° de PS', validators=[Length(max=8), DataRequired()])
    telefono_1 = StringField('Tel.', validators=[Length(max=15)])
    telefono_2 = StringField('Tel. alt.', validators=[Length(max=15)])
    dependencia = MySelectField('Efector', choices=[(0, '')], coerce=int)
    receta = FileField('Receta', validators=[FileRequired(), FileAllowed(['jpg', 'png', 'gif', 'pdf', 'jpeg', 'tif', 'tiff'], 'Sólo imágenes o archivos PDF')], render_kw={'data-default-value': 'No se seleccionaron archivos'})


