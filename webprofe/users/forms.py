from flask_wtf import FlaskForm
from wtforms import SelectField, ValidationError
from wtforms import StringField, SelectField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, ValidationError, Email, EqualTo
from webprofe.main.recaptcha3 import Recaptcha3Field
from webprofe.models import User


class MySelectField(SelectField):
    def pre_validate(self, RegistrationForm):
        if self.data == 0:
            raise ValidationError("Debe seleccionar efector")
        

class RegistrationForm(FlaskForm):
    nombre = StringField('Nombres', validators=[DataRequired(), Length(max=60)])
    apellido = StringField('Apellido', validators=[DataRequired(), Length(max=60)])
    dni = StringField('Nro. DNI', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Length(max=120), Email("Debe ingresar un email válido")])
    dependencia = MySelectField('Dependencia', choices=[(0, '')], coerce=int)
    password = PasswordField('Contraseña', validators=[DataRequired(), Length(max=60)])
    confirm_password = PasswordField('Confirmar contraseña', validators=[DataRequired(), Length(max=60), EqualTo('password')])
    recaptcha = Recaptcha3Field(action="SubmitRegister", execute_on_load=True)
    submit = SubmitField('Registrar')

    def validate_dni(self, dni):
        user = User.query.filter_by(dni=int(dni.data.strip())).first()
        if user:
            raise ValidationError('Ya existe un usuario creado con ese DNI.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data.strip()).first()
        if user:
            raise ValidationError('Ya existe un usuario creado con ese email.')
        
    

class LoginForm(FlaskForm):
    dni = StringField('Nro. DNI', validators=[DataRequired()])
    password = PasswordField('Contraseña', validators=[DataRequired()])
    remember = BooleanField('Recordar inicio de sesión')
    submit = SubmitField('Ingresar')


class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email("Debe ingresar un email válido.")])
    submit = SubmitField('Solicitar nueva contraseña')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('No hay cuenta con ese email.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Contraseña', validators=[DataRequired()])
    confirm_password = PasswordField('Confirmar contraseña', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Restablecer contraseña')