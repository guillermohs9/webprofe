from flask import current_app
import os
from pyhanko import stamp
# from pyhanko.pdf_utils import text
# from pyhanko.pdf_utils.font import opentype
from pyhanko.pdf_utils.incremental_writer import IncrementalPdfFileWriter
from pyhanko.sign import fields, signers


def sign_pdf(user, pdf):
    signer = signers.SimpleSigner.load(
        os.path.join(current_app.root_path, "static", "certs", str(user.id) + ".key"),
        os.path.join(current_app.root_path, "static", "certs", str(user.id) + ".pem"), 
    )
    w = IncrementalPdfFileWriter(pdf)
    fields.append_signature_field(
        w, sig_field_spec=fields.SigFieldSpec(
            'Signature', box=(150, 100, 450, 140)
        )
    )

    meta = signers.PdfSignatureMetadata(field_name='Signature')
    pdf_signer = signers.PdfSigner(
        meta, signer=signer, stamp_style=stamp.TextStampStyle(
            # the 'signer' and 'ts' parameters will be interpolated by pyHanko, if present
            stamp_text=f'DOCUMENTO FIRMADO DIGITALMENTE\nFirmado por: {user.nombre} {user.apellido} - {user.dni}\nTime: %(ts)s',
            # text_box_style=text.TextBoxStyle(
            #     font=opentype.GlyphAccumulatorFactory(os.path.join(current_app.root_path, 'static/uploads', 'NotoSans-Regular.ttf'))
            # )
        ),
    )

    pdf_signer.sign_pdf(w, in_place=True)

