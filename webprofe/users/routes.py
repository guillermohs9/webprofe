from flask import render_template, request, Blueprint, url_for, redirect, flash, abort
from flask_login import current_user, login_required, login_user, logout_user
from webprofe import db, bcrypt
from webprofe.models import Dependencia, User
from webprofe.users.forms import RegistrationForm, LoginForm, RequestResetForm, ResetPasswordForm
from webprofe.users.utils import is_safe_url, solo_admins, send_reset_email, generar_certs_user


users = Blueprint('users', __name__)


@users.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.inicio'))
    form = RegistrationForm()
    form.dependencia.choices.extend([(x.id, x.nombre) for x in Dependencia.query.order_by(Dependencia.nombre.asc()).all()])
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(dependencia_id=form.dependencia.data, 
                    email=form.email.data.strip(), 
                    nombre=form.nombre.data.strip().title(), 
                    apellido=form.apellido.data.strip().title(), 
                    dni=int(form.dni.data.strip()),
                    password=hashed_password, 
                    rol_id=2)
        db.session.add(user)
        db.session.commit()
        flash("<span class='fs-5'>El usuario ha sido creado con éxito</span>", 'success')
        if not generar_certs_user(user):
            flash("<span class='fs-5'>Error al generar claves de usuario</span>", 'danger')
        return redirect(url_for('main.inicio'))
    return render_template('register.html', title='Registrar nuevo usuario', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if current_user.baja:
            flash("<span class='fs-5'>Su usario ha sido dado de baja</span>", 'danger')
            return redirect(url_for('users.logout'))
        else:
            return redirect(url_for('main.inicio'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(dni=form.dni.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            if user.baja:
                flash("<span class='fs-5'>Su usario ha sido dado de baja</span>", 'danger')
                return redirect(request.url)
            else:
                login_user(user, remember=form.remember.data)
                db.session.commit()
                next_page = request.args.get('next')
                if not is_safe_url(next_page):
                    abort(400)
                return redirect(next_page or url_for('main.inicio'))
        else:
            flash("<span class='fs-5'>Inicio de sesión fallido. Verifique e-mail y contraseña</span>", 'danger')
    return render_template('login.html', title='Iniciar sesión', form=form)


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.inicio'))


@users.route("/user/<int:user_id>/eliminar", methods=['GET'])
@login_required
@solo_admins
def eliminar_usuario(user_id):
    user = User.query.get_or_404(user_id)
    if user.baja:
        user.baja = False
        db.session.commit()
        flash(f'<span class="fs-5">Se activó al usuario {user.nombre} {user.apellido}</span>', 'success')
    else:
        user.baja = True
        db.session.commit()
        flash(f'<span class="fs-5">Se dio de baja al usuario {user.nombre} {user.apellido}</span>', 'success')
    return redirect(url_for('users.ver_usuarios'))


@users.route("/user/<int:user_id>/modificar", methods=['GET'])
@login_required
@solo_admins
def modificar_usuario(user_id):
    user = User.query.get_or_404(user_id)
    if user.rol_id == 1:
        user.rol_id = 2
    else:
        user.rol_id = 1
    flash(f'<span class="fs-5">Se modificó el usuario {user.nombre} {user.apellido}</span>', 'success')
    db.session.commit()
    return redirect(url_for('users.ver_usuarios'))


@users.route("/user/<int:user_id>/renovar_certs", methods=['GET'])
@login_required
@solo_admins
def renovar_certs(user_id):
    user = User.query.get_or_404(user_id)
    generar_certs_user(user)
    flash(f'<span class="fs-5">Se renovaron los certificados de {user.nombre} {user.apellido}</span>', 'success')
    return redirect(url_for('users.ver_usuarios'))


@users.route("/users", methods=['GET', 'POST'])
@login_required
@solo_admins
def ver_usuarios():
    users = User.query.all()
    # users = User.query.filter_by(User.rol_id > 1).all()
    return render_template('users.html', title='Ver usuarios', users=users)


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.inicio'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if not user:
            flash('<span class="fs-5">No existen usuarios registrados con ese correo electrónico.</span>', 'info')
            return redirect(url_for('users.login'))
        send_reset_email(user)
        flash(f'<span class="fs-5">Se ha enviado un email a {user.email} con instrucciones para restablecer la contraseña. Revisar carpeta de correo no deseado.</span>', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Restablecer contraseña', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.inicio'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('<span class="fs-5">Token inválido o expirado.</span>', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('<span class="fs-5">Su contraseña ha sido actualizada. Ya puede iniciar sesión.</span>', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Retablecer contraseña', form=form)


# @users.route("/hacer_title_case", methods=['GET', 'POST'])
# def hacer_title_case():
#     users = User.query.all()
#     for u in users:
#         u.nombre = u.nombre.title()
#         u.apellido = u.apellido.title()
#     db.session.commit()
#     return redirect(url_for('main.inicio'))
