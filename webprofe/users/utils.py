from __future__ import print_function
from functools import wraps
from flask import url_for, current_app, request, abort
from flask_login import current_user
from webprofe import db
from urllib.parse import urlparse, urljoin
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import subprocess
from datetime import datetime
import os
from OpenSSL import crypto


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


def solo_admins(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.rol_id > 1:
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


def send_reset_email(user):
    token = user.get_reset_token()
    html_body = f'''<p>Para restablecer su contraseña, visite <a href="{url_for('users.reset_token', token=token, _external=True)}">este link</a></p>
<p>Si usted no solicitó restablecer su contraseña, puede ignorar este correo.</p>
'''
    send_email('Restablezca su contraseña', 'pfisnqn@gmail.com', user.email, html_body)


def send_email(subject, sender, to, html_body):
    message = Mail(
        from_email=sender,
        to_emails=to,
        subject=subject,
        html_content=html_body)
    try:
        sg = SendGridAPIClient(current_app.config['SENDGRID_API_KEY'])
        response = sg.send(message)
        # print(response.status_code)
        # print(response.body)
        # print(response.headers)
    except Exception as e:
        print(e.message)


def generar_certs_user(user):
    comm = ['openssl', 'req', '-x509', '-newkey', 'rsa:4096', '-keyout', f'{os.path.join(current_app.root_path, "static", "certs", str(user.id) + ".key")}', '-out', f'{os.path.join(current_app.root_path, "static", "certs", str(user.id) + ".pem")}', '-days', '3650', '-nodes', '-subj', f"/C=AR/ST=Neuquén/L=Neuquén/O=Ministerio de Salud/OU=Programa Federal Incluir Salud/CN={user.nombre} {user.apellido}/emailAddress={user.email}"]
    sp = subprocess.run(comm)
    if sp.stdout:
        print(f'stdout pdfunite: {sp.stdout}')
    if sp.stderr:
        print(f'stderr pdfunite: {sp.stderr}')
    if sp.returncode == 0:
        user.fecha = datetime.utcnow()
        db.session.commit()
        return True
    else:
        return False
    

def validate_certs(user):
    # open it, using password. Supply/read your own from stdin.
    p12 = crypto.load_pkcs12(open("/path/to/cert.p12", 'rb').read(), b'')

    # get various properties of said file.
    # note these are PyOpenSSL objects, not strings although you
    # can convert them to PEM-encoded strings.
    p12.get_certificate()     # (signed) certificate object
    p12.get_privatekey()      # private key.
    p12.get_ca_certificates() # ca chain.
