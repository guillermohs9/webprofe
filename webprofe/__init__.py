from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from webprofe.config import Config, ProductionConfig, StagingConfig, DevelopmentConfig, TestingConfig
from flask_migrate import Migrate
from flask_moment import Moment
import flask_excel as excel
from flask_bcrypt import Bcrypt
from flask_login import LoginManager


db = SQLAlchemy()
migrate = Migrate()
moment = Moment()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message = "<span class='fs-5'>Por favor inicie sesión</span>"
login_manager.login_message_category = 'info'

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)
    moment.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    excel.init_excel(app)
    from webprofe.main.routes import main
    from webprofe.users.routes import users
    from webprofe.errors.handlers import errors
    app.register_blueprint(errors)
    app.register_blueprint(main)
    app.register_blueprint(users)

    # with app.app_context():
    #     db.create_all()

    return app
