from webprofe import db, login_manager
from flask_login import UserMixin
from flask import current_app
from datetime import timezone, timedelta, datetime
import zoneinfo
import jwt


def xstr(s):
    if s is None:
        return ''
    return str(s)


ARG = zoneinfo.ZoneInfo('America/Buenos_Aires')
GMT = zoneinfo.ZoneInfo('UTC')

class MotivosBajas(db.Model):
    __tablename__ = 'MotivosBajas'
    Motivo = db.Column(db.Integer, nullable=False, primary_key=True)
    Desc_Mov = db.Column(db.String(80, 'utf8mb4_unicode_ci'), nullable=False)


class TablaGrupos(db.Model):
    __tablename__ = 'TablaGrupos'
    Codigo = db.Column(db.String(4), nullable=False, primary_key=True)
    Descripcion = db.Column(db.String(80, 'utf8mb4_unicode_ci'), nullable=False)


class cod_OS(db.Model):
    __tablename__ = 'cod_OS'
    CODOS = db.Column(db.String(12, 'utf8mb4_unicode_ci'), nullable=False, primary_key=True)
    DESCOS = db.Column(db.String(100, 'utf8mb4_unicode_ci'))


class Provincia(db.Model):
    __tablename__ = 'PROVINCIA'
    Provincia = db.Column(db.Integer, nullable=False, primary_key=True)
    Detalle = db.Column(db.String(100, 'utf8mb4_unicode_ci'))


class Departamento(db.Model):
    __tablename__ = 'DEPARTAMENTO'
    Provincia = db.Column(db.Integer, nullable=False)
    Departamento = db.Column(db.Integer, nullable=False, primary_key=True)
    Nombre_Dep = db.Column(db.String(510, 'utf8mb4_unicode_ci'))


class Localidad(db.Model):
    __tablename__ = 'LOCALIDAD'
    Provincia = db.Column(db.Integer, nullable=False)
    Departamento = db.Column(db.Integer, nullable=False)
    Localidad = db.Column(db.Integer, nullable=False, primary_key=True)
    CódigoPostal = db.Column(db.String(510, 'utf8mb4_unicode_ci'))
    Nombre_Loc = db.Column(db.String(510, 'utf8mb4_unicode_ci'))


class Bajas(db.Model):
    __tablename__ = 'BAJAS'
    clave_excaja = db.Column(db.Integer, nullable=False)
    clave_tipo = db.Column(db.Integer, nullable=False)
    clave_numero = db.Column(db.Integer, nullable=False)
    clave_coparticipe = db.Column(db.Integer, nullable=False)
    clave_parentesco = db.Column(db.Integer, nullable=False)
    leyaplicada = db.Column(db.String(4, 'utf8mb4_unicode_ci'))
    apenom = db.Column(db.String(60, 'utf8mb4_unicode_ci'))
    sexo = db.Column(db.String(2, 'utf8mb4_unicode_ci'), nullable=False)
    tipo_doc = db.Column(db.String(4, 'utf8mb4_unicode_ci'))
    numero_doc = db.Column(db.Integer, primary_key=True, autoincrement=False)
    fe_nac = db.Column(db.DateTime, nullable=False)
    fechalta = db.Column(db.DateTime, nullable=False)
    fechbaja = db.Column(db.DateTime, nullable=False)
    motivo = db.Column(db.Integer)
    cug_pcia = db.Column(db.Integer, nullable=False)
    cug_dpto = db.Column(db.Integer)
    cug_loc = db.Column(db.Integer)
    codosoc = db.Column(db.String(12, 'utf8mb4_unicode_ci'), nullable=False)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Profe(db.Model):
    __tablename__ = 'PROFE'
    Clave_ExCaja = db.Column(db.Integer, nullable=False)
    Clave_Tipo = db.Column(db.Integer, nullable=False)
    Clave_Numero = db.Column(db.Integer, nullable=False)
    Clave_Coparticipe = db.Column(db.Integer, nullable=False)
    Clave_Parentesco = db.Column(db.Integer, nullable=False)
    LeyAplicada = db.Column(db.String(4, 'utf8mb4_unicode_ci'))
    ApeNom = db.Column(db.String(60, 'utf8mb4_unicode_ci'), nullable=False)
    Sexo = db.Column(db.String(2, 'utf8mb4_unicode_ci'))
    EstCivil = db.Column(db.Integer)
    Tipo_Doc = db.Column(db.String(4, 'utf8mb4_unicode_ci'), nullable=False)
    Numero_Doc = db.Column(db.Integer, nullable=False, primary_key=True)
    Fe_Nac = db.Column(db.DateTime)
    Incapacidad = db.Column(db.Integer)
    Fech_Alta = db.Column(db.DateTime, nullable=False)
    Dom_Calle = db.Column(db.String(100, 'utf8mb4_unicode_ci'))
    Dom_Nro = db.Column(db.Integer)
    Dom_Piso = db.Column(db.Integer)
    Dom_Dpto = db.Column(db.String(16, 'utf8mb4_unicode_ci'))
    Cod_Pos = db.Column(db.String(16, 'utf8mb4_unicode_ci'))
    Cug_Pcia = db.Column(db.Integer, nullable=False)
    Cug_Dpto = db.Column(db.Integer, nullable=False)
    Cug_loc = db.Column(db.Integer, nullable=False)

    telefonos = db.relationship('Telefono', foreign_keys='Telefono.PROFE_Numero_Doc', backref='paciente', lazy=True)
    
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def calculateAge(self):
        today = datetime.now().replace(tzinfo=timezone.utc).astimezone(ARG).date()
        try:
            birthday = self.Fe_Nac.date().replace(year = today.year)
    
        # raised when birth date is February 29
        # and the current year is not a leap year
        except ValueError:
            birthday = self.Fe_Nac.date().replace(year = today.year,
                    month = self.Fe_Nac.date().month + 1, day = 1)
    
        if birthday > today:
            return today.year - self.Fe_Nac.date().year - 1
        else:
            return today.year - self.Fe_Nac.date().year


    def anotaciones(self):
        anotaciones = Anotacion.query \
            .filter_by(PROFE_Numero_Doc=self.Numero_Doc) \
                .all()
        return anotaciones
    
    def numeros_telefonicos_obj(self):
        telefonos = Telefono.query \
            .filter_by(PROFE_Numero_Doc=self.Numero_Doc) \
                .all()
        return telefonos

    def numeros_telefonicos(self):
        telefonos = Telefono.query \
            .filter_by(PROFE_Numero_Doc=self.Numero_Doc) \
                .all()
        if telefonos:
            return [t.Numero_tel for t in telefonos]
        else:
            return None
        # if telefonos:
        #     lista_tels = json.loads(telefonos.Numeros_tel)
        #     for x in range(len(lista_tels)):
        #         if not lista_tels[x].startswith('+54'):
        #             lista_tels[x] = f'+54{lista_tels[x]}'
        #     return lista_tels
        # else:
        #     return None

    def lentes_pedidos(self):
        pedidos = Solicitud.query \
            .filter_by(numero_doc=self.Numero_Doc) \
                .all()
        return pedidos if pedidos else None
    
    def get_localidad(self):
        localidad = Localidad.query \
            .join(Departamento, Departamento.Departamento == Localidad.Departamento) \
                .join(Provincia, Provincia.Provincia == Localidad.Provincia) \
                    .filter(Provincia.Provincia == self.Cug_Pcia) \
                        .filter(Departamento.Departamento == self.Cug_Dpto) \
                            .filter(Localidad.Localidad == self.Cug_loc) \
                                .first()
        return localidad.Nombre_Loc
    
    def to_dict(self):
        return {
            'nombre' : xstr(self.ApeNom).strip(),
            'numero_doc' : self.Numero_Doc,
            'fe_nac' : self.Fe_Nac.strftime("%d/%m/%Y"),
            'beneficio' : xstr(self.Clave_ExCaja) + xstr(self.Clave_Tipo) + xstr(self.Clave_Numero) + xstr(
                self.Clave_Coparticipe) + xstr(self.Clave_Parentesco),
            'fecha_alta' : self.Fech_Alta.strftime("%d/%m/%Y"),
            'domicilio' : xstr(self.Dom_Calle).strip() + " " + xstr(self.Dom_Nro).strip() + " Piso " + xstr(self.Dom_Piso).strip() + " Dpto " + xstr(self.Dom_Dpto).strip(),
            'localidad' : xstr(self.get_localidad()),
            'tels' : " - ".join([i for i in self.numeros_telefonicos()]) if self.numeros_telefonicos() else ''
        }
        

class Profe_Caps(db.Model):
    __tablename__ = 'PROFE_CAPS'
    Clave_ExCaja = db.Column(db.Integer, nullable=False)
    Clave_Tipo = db.Column(db.Integer, nullable=False)
    Clave_Numero = db.Column(db.Integer, nullable=False)
    Clave_Coparticipe = db.Column(db.Integer, nullable=False)
    Clave_Parentesco = db.Column(db.Integer, nullable=False)
    LeyAplicada = db.Column(db.String(4, 'utf8mb4_unicode_ci'))
    ApeNom = db.Column(db.String(60, 'utf8mb4_unicode_ci'), nullable=False)
    Sexo = db.Column(db.String(2, 'utf8mb4_unicode_ci'))
    EstCivil = db.Column(db.Integer)
    Tipo_Doc = db.Column(db.String(4, 'utf8mb4_unicode_ci'), nullable=False)
    Numero_Doc = db.Column(db.Integer, nullable=False, primary_key=True)
    Fe_Nac = db.Column(db.DateTime)
    Incapacidad = db.Column(db.Integer, nullable=False)
    Fech_Opc = db.Column(db.DateTime, nullable=False)
    Tipo_Opc = db.Column(db.String(4, 'utf8mb4_unicode_ci'), nullable=False)
    Dom_Calle = db.Column(db.String(100, 'utf8mb4_unicode_ci'))
    Dom_Nro = db.Column(db.Integer)
    Dom_Piso = db.Column(db.Integer)
    Dom_Dpto = db.Column(db.String(6, 'utf8mb4_unicode_ci'))
    Cod_Pos = db.Column(db.String(16, 'utf8mb4_unicode_ci'))
    Cug_Pcia = db.Column(db.Integer, nullable=False)
    Cug_Dpto = db.Column(db.Integer, nullable=False)
    Cug_loc = db.Column(db.Integer, nullable=False)
    Nro_Cap = db.Column(db.Integer, nullable=False)
    usr = db.Column(db.String(16, 'utf8mb4_unicode_ci'), nullable=False)


# class Telefonos(db.Model):
#     __tablename__ = 'TELEFONOS'
#     Numero_doc = db.Column(db.Integer, primary_key=True, nullable=False)
#     Numeros_tel = db.Column(db.Text)
#     ip = db.Column(db.Text)


class Telefono(db.Model):
    __tablename__ = 'TELEFONO'
    id = db.Column(db.Integer, primary_key=True)
    PROFE_Numero_Doc = db.Column(db.Integer, db.ForeignKey('PROFE.Numero_Doc'), nullable=False)
    Numero_tel = db.Column(db.String(15))


class Anotacion(db.Model):
    __tablename__ = 'ANOTACION'
    id = db.Column(db.Integer, primary_key=True)
    PROFE_Numero_Doc = db.Column(db.Integer, db.ForeignKey('PROFE.Numero_Doc'), nullable=False)
    fecha = db.Column(db.DateTime, default=datetime.utcnow)
    texto = db.Column(db.String(30))
    user_id = db.Column(db.Integer, db.ForeignKey('USER.id'), nullable=False)

    def obtener_paciente(self):
        return Profe.query.filter(Profe.Numero_Doc == self.PROFE_Numero_Doc).first()

    def fecha_printable(self):
        return self.fecha.strftime("%d/%m/%Y")


class Solicitud(db.Model):
    __tablename__ = 'SOLICITUD'
    id = db.Column(db.Integer, primary_key=True)
    numero_ps = db.Column(db.Integer, nullable=False)
    numero_doc = db.Column(db.Integer, nullable=False)
    nombre_paciente = db.Column(db.String(200), nullable=False)
    # telefonos_paciente = db.Column(db.String(300), nullable=False)
    receta = db.Column(db.String(200))
    dependencia_id = db.Column(db.Integer, db.ForeignKey('DEPENDENCIA.id'), nullable=False)
    fecha = db.Column(db.DateTime, default=datetime.utcnow)
    gestionada = db.Column(db.Boolean, default=False)


    def ley_aplicada(self):
        resultado = Profe.query.filter(Profe.Numero_Doc == self.numero_doc).first()
        return TablaGrupos.query.filter_by(Codigo=resultado.LeyAplicada).first().Descripcion

    def edad(self):
        resultado = Profe.query.filter(Profe.Numero_Doc == self.numero_doc).first()
        return resultado.calculateAge()
    
    def telefonos_paciente(self):
        paciente = Profe.query.filter(Profe.Numero_Doc == self.numero_doc).first()
        tels = paciente.numeros_telefonicos()
        if tels:
            return ' - '.join(tels)
        else:
            return None
        
    def __str__(self):
        return f'Solicitud {self.id} paciente {self.nombre_paciente} - DNI {self.numero_doc}'

    def fecha_printable(self):
        return self.fecha.strftime("%d/%m/%Y")


class Dependencia(db.Model):
    __tablename__ = 'DEPENDENCIA'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(200))
    solicitudes = db.relationship('Solicitud', foreign_keys='Solicitud.dependencia_id', backref='dependencia', lazy=True)
    users = db.relationship('User', foreign_keys='User.dependencia_id', backref='dependencia', lazy=True)


@login_manager.user_loader
def load_user(user_id):
    user = User.query.get(int(user_id))
    if user.baja or (user.fecha + timedelta(days=current_app.config['CADUCIDAD_CERT'])) < datetime.utcnow():
        return None
    else:
        return User.query.get(int(user_id))


class Rol(db.Model):
    __tablename__ = 'ROL'
    id = db.Column(db.Integer, primary_key=True)
    descripcion = db.Column(db.String(20), nullable=False)
    users = db.relationship('User', foreign_keys='User.rol_id', backref='rol', lazy=True)


class User(db.Model, UserMixin):
    __tablename__ = 'USER'
    id = db.Column(db.Integer, primary_key=True)
    dependencia_id = db.Column(db.Integer, db.ForeignKey('DEPENDENCIA.id'), nullable=False)
    nombre = db.Column(db.String(60), nullable=False)
    apellido = db.Column(db.String(60), nullable=False)
    dni = db.Column(db.Integer, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    baja = db.Column(db.Boolean, default=False)
    rol_id = db.Column(db.Integer, db.ForeignKey('ROL.id'), nullable=False)
    fecha = db.Column(db.DateTime, default=datetime.utcnow)
    anotaciones = db.relationship('Anotacion', foreign_keys='Anotacion.user_id', backref='user', lazy=True)

    def get_reset_token(self, expires_sec=1800):
        reset_token = jwt.encode(
            {
                "user_id": self.id,
                "exp": datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(ARG)
                    + timedelta(seconds=expires_sec)
            },
            current_app.config['SECRET_KEY'],
            algorithm="HS256"
        )
        return reset_token

    @staticmethod
    def verify_reset_token(token):
        try:
            data = jwt.decode(
                token,
                current_app.config['SECRET_KEY'],
                leeway=timedelta(seconds=10),
                algorithms=["HS256"]
            )
            id = data.get('user_id')
            user = User.query.get(id)
            if user:
                return user
            else:
                return None
        except:
            return None

    def fecha_printable(self):
        return self.fecha.strftime("%d/%m/%Y")

    def fecha_venc_cert(self):
        return (self.fecha + timedelta(days=current_app.config['CADUCIDAD_CERT'])).strftime("%d/%m/%Y")

    def __str__(self):
        return f"{self.nombre.title()}"