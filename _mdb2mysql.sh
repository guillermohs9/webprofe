#!/bin/bash
# found this here: http://stackoverflow.com/questions/5722544/how-can-i-convert-an-mdb-access-file-to-mysql-or-plain-sql-file
#
# Create DB like this: CREATE DATABASE xyc;
# To invoke it simply call it like this:
# ./mdbconvert.sh accessfile.mdb mysqldatabasename

TABLES=$(mdb-tables -1 $1)

MUSER="softalq"
MPASS="softalq2020"
MDB="$2"

MYSQL=$(which mysql)

for t in $TABLES
do
    $MYSQL -u $MUSER -p$MPASS $MDB -e "DROP TABLES IF EXISTS $t"
done

mdb-schema $1 mysql | $MYSQL -u $MUSER -p$MPASS $MDB

for t in $TABLES
do
    mdb-export -D '%Y-%m-%d %H:%M:%S' -I mysql $1 $t | $MYSQL -u $MUSER -p$MPASS $MDB
done
