#!/bin/bash
# found this here: http://stackoverflow.com/questions/5722544/how-can-i-convert-an-mdb-access-file-to-mysql-or-plain-sql-file
#
# Create DB like this: CREATE DATABASE xyc;
# To invoke it simply call it like this:
#./mdb_to_mysql.sh database.mdb > data.sql
#./mdb_to_mysql.sh database.mdb | mysql destination-db -u user -p

MUSER="pfisnqn"
MPASS="passwordpfisNQN2020"
MDB="$2"

MYSQL=$(which mysql)

TABLES=$(mdb-tables -1 $1)

for t in $TABLES
do
    echo "DROP TABLE IF EXISTS $t;"
done

mdb-schema $1 mysql

for t in $TABLES
do
    mdb-export -D '%Y-%m-%d %H:%M:%S' -I mysql $1 $t
done