"""empty message

Revision ID: a2fd54f5e99c
Revises: 64e6ceb7a859
Create Date: 2024-10-08 15:06:07.261698

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a2fd54f5e99c'
down_revision = '64e6ceb7a859'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('ANOTACION',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('PROFE_Numero_Doc', sa.Integer(), nullable=False),
    sa.Column('fecha', sa.DateTime(), nullable=True),
    sa.Column('texto', sa.String(length=30), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['USER.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('ANOTACION')
    # ### end Alembic commands ###
