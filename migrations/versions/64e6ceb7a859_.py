"""empty message

Revision ID: 64e6ceb7a859
Revises: 62d41514bce6
Create Date: 2023-09-19 14:54:23.366834

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '64e6ceb7a859'
down_revision = '62d41514bce6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('TELEFONOS')
    # with op.batch_alter_table('BAJAS', schema=None) as batch_op:
    #     batch_op.alter_column('numero_doc',
    #            existing_type=sa.INTEGER(),
    #            nullable=False,
    #            autoincrement=False,
    #            existing_server_default=sa.text('(NULL)'))

    # with op.batch_alter_table('PROFE', schema=None) as batch_op:
    #     batch_op.alter_column('Dom_Dpto',
    #            existing_type=sa.VARCHAR(length=6),
    #            type_=sa.String(length=16, collation='utf8mb4_unicode_ci'),
    #            existing_nullable=True,
    #            existing_server_default=sa.text('(NULL)'))

    # with op.batch_alter_table('PROFE_CAPS', schema=None) as batch_op:
    #     batch_op.add_column(sa.Column('Fe_Nac', sa.DateTime(), nullable=True))
    #     batch_op.alter_column('Tipo_Opc',
    #            existing_type=sa.VARCHAR(length=2),
    #            type_=sa.String(length=4, collation='utf8mb4_unicode_ci'),
    #            existing_nullable=False)
    #     batch_op.drop_column('FeNac')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # with op.batch_alter_table('PROFE_CAPS', schema=None) as batch_op:
    #     batch_op.add_column(sa.Column('FeNac', sa.INTEGER(), server_default=sa.text('(NULL)'), nullable=True))
    #     batch_op.alter_column('Tipo_Opc',
    #            existing_type=sa.String(length=4, collation='utf8mb4_unicode_ci'),
    #            type_=sa.VARCHAR(length=2),
    #            existing_nullable=False)
    #     batch_op.drop_column('Fe_Nac')

    # with op.batch_alter_table('PROFE', schema=None) as batch_op:
    #     batch_op.alter_column('Dom_Dpto',
    #            existing_type=sa.String(length=16, collation='utf8mb4_unicode_ci'),
    #            type_=sa.VARCHAR(length=6),
    #            existing_nullable=True,
    #            existing_server_default=sa.text('(NULL)'))

    # with op.batch_alter_table('BAJAS', schema=None) as batch_op:
    #     batch_op.alter_column('numero_doc',
    #            existing_type=sa.INTEGER(),
    #            nullable=True,
    #            autoincrement=False,
    #            existing_server_default=sa.text('(NULL)'))

    op.create_table('TELEFONOS',
    sa.Column('Numero_doc', sa.INTEGER(), nullable=False),
    sa.Column('Numeros_tel', sa.TEXT(), nullable=True),
    sa.Column('ip', sa.TEXT(), nullable=True),
    sa.PrimaryKeyConstraint('Numero_doc')
    )
    # ### end Alembic commands ###
