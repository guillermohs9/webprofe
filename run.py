from webprofe import create_app
import os
from flask import current_app
from datetime import datetime, timedelta

app = create_app()

@app.context_processor
def appinfo():
    mod_timestamp = datetime.fromtimestamp(os.path.getmtime(os.path.join(current_app.root_path, 'static', 'fecha')))
    fecha_form = mod_timestamp - timedelta(days=45)
    return dict(mod_timestamp=mod_timestamp, fecha_form=fecha_form)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=os.name == 'nt')
